### ***** Importar librerias ***** ###

import psycopg2
from config import config
import numpy as np
import pandas as pd
import datetime
from datetime import timedelta

# ------------------------------------------------------------------------
# Determinar fecha actual, desde cuando se relizara la busqueda en Tweet
# e Insertar las descargas a la Base de datos 
# ------------------------------------------------------------------------

### ***** Fecha actual ***** ###
d = datetime.date.today()
d=str(d)

### ***** Querys para insertar las descargas a la Base de Datos ***** ###
# Español
sql_es="""INSERT INTO tweets_data_fech_act_es(id, conversation_id, created_at, date, time, timezone, user_id, username, name,
                                place, tweet, mentions, urls, photos, replies_count, retweets_count, likes_count, hashtags, cashtags,
                                link, retweet, quote_url, video, near, geo, source, user_rt_id, user_rt, retweet_id, reply_to, retweet_date)
                                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s,
                                %s, %s, %s, %s, %s);"""
# Ingles 
sql_en="""INSERT INTO tweets_data_fech_act_en(id, conversation_id, created_at, date, time, timezone, user_id, username, name,
                                place, tweet, mentions, urls, photos, replies_count, retweets_count, likes_count, hashtags, cashtags,
                                link, retweet, quote_url, video, near, geo, source, user_rt_id, user_rt, retweet_id, reply_to, retweet_date)
                                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s,
                                %s, %s, %s, %s, %s);"""

### ***** Ruta completa donde estan los archivos .CSV que se insertaran a l BDD
path_es='/home/jorgeda/pruebas_jorge/BDD/tweets_español/'+d+'/'+'All_Data-tweets_español-'+d+'.csv.gz'
path_en='/home/jorgeda/pruebas_jorge/BDD/tweets_ingles/'+d+'/'+'All_Data-tweets_ingles-'+d+'.csv.gz'

###***** Conectar a la Base de Datos *****###
conn = None

try:
    l = ['id', 'conversation_id', 'created_at', 'date', 'time', 'timezone', 'user_id', 'username', 'name', 'place', 'tweet', 'mentions', 'urls', 'photos', 'replies_count', 'retweets_count', 'likes_count', 'hashtags', 'cashtags','link', 'retweet', 'quote_url', 'video', 'near', 'geo', 'source', 'user_rt_id', 'user_rt', 'retweet_id', 'reply_to', 'retweet_date']
    # Configuración de la Base de Datos 
    params = config()
    # Conectar a la Base de datos  PostgreSQL
    conn = psycopg2.connect(**params)
    # Crear nuevo cursor
    cur = conn.cursor()
    # insetar datos a base de datos español
    df_es = pd.read_csv(path_es, engine='python', compression='gzip')
    df_es = df_es.replace(np.nan, '', regex=True)
    df_es= df_es.astype(str)
    l_n_es = []
    for i in df_es.columns:
        if i not in l:
            l_n_es.append(i)
    df_es = df_es.drop(columns = l_n_es)
    for i in range(len(df_es)):
        list_es=df_es.iloc[i].tolist()
        cur.execute(sql_es, list_es)
        conn.commit()

    # insertar datos a  base de datos ingles
    df_en= pd.read_csv(path_en, engine='python', compression='gzip')
    df_en = df_en.replace(np.nan, '', regex=True)
    df_en= df_en.astype(str)
    l_n_en = []
    for i in df_en.columns:
        if i not in l:
            l_n_en.append(i)
    df_en = df_en.drop(columns = l_n_en)
    for j in range(len(df_en)):
        list_en=df_en.iloc[j].tolist()
        cur.execute(sql_en, list_en)
        conn.commit()
    # Cerrar Cursor 
    cur.close()
except (Exception, psycopg2.DatabaseError) as error:
    print(error)
finally:
    if conn is not None:
	# Cerrar conexión
        conn.close()
