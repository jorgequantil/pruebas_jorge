from flask import Flask, request, jsonify
import psycopg2
from config import config
import numpy as np
import pandas as pd

# Iniciar app
app = Flask(__name__)


# GET tweets en Ingles y Español
@app.route('/', methods = ['GET'])

def get_tweets():
    
    try:
        # Leer configuración de la base de datos
        params = config()
        # Conectarse a la base de datos PostgreSQL
        conn = psycopg2.connect(**params)
        # Crear cursor para hacer las consultas
        cur_en = conn.cursor()
        cur_es = conn.cursor()

        #Query para las Consultas
        Query_en = "SELECT fecha_rastreo, id_unique, date, username, name, tweet, retweets_count FROM tweets_data_fech_act_en WHERE fecha_rastreo = (SELECT max(fecha_rastreo) FROM tweets_data_fech_act_en);"
        Query_es = "SELECT fecha_rastreo, id_unique, date, username, name, tweet, retweets_count FROM tweets_data_fech_act_es WHERE fecha_rastreo = (SELECT max(fecha_rastreo) FROM tweets_data_fech_act_es);"

        #Ejecutar las consultas y guardarla en una lista
        cur_en.execute(Query_en)
        cur_es.execute(Query_es)
        lista_consulta_ingles = cur_en.fetchall()
        lista_consulta_espaniol = cur_es.fetchall()

        #Crear Dataframe con cada lista
        df_en=pd.DataFrame(lista_consulta_ingles, columns=['fecha_rastreo','id_unique', 'date', 'username', 'name', 'tweet', 'retweets_count'])
        df_es=pd.DataFrame(lista_consulta_espaniol, columns=['fecha_rastreo','id_unique', 'date', 'username', 'name', 'tweet', 'retweets_count'])

        # Fecha de consulta
        fecha_consulta = df_en['fecha_rastreo'][0]

        #Numero de Tweets descargados
        num_tweets_en = df_en.shape[0]
        num_tweets_es = df_es.shape[0]

        #Crear archivos JSON con cada Dataframe
        tweets_espaniol = df_es.to_json(orient='records')
        tweets_ingles = df_en.to_json(orient='records')

        #Crear diccionarios com los JSON, fechas de consulta y número de tweets descargados
        dict_tweets = {'fecha_consulta': str(fecha_consulta), 'numero_tweets_espaniol': num_tweets_es, 'numero_tweets_ingles': num_tweets_en, 'tweets_espaniol': tweets_espaniol, 'tweets_ingles': tweets_ingles}

        cur_en.close()
        cur_es.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    
    
    return dict_tweets


if __name__=="__main__":
    app.run(host='173.230.157.249', debug=True)
    #app.run(host='localhost', debug=True)
