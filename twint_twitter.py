### ***** Preparar Ruta de Modulo Twint ***** ### 
import sys
sys.path.append("/home/jorgeda/twint/")

### ***** Importar librerias ***** ###
import twint
import datetime
from datetime import timedelta
import pandas as pd
import os
import glob
import random
import time
from capturing_error import *
from global_path import *

### ***** Resolver problemas con PC portatil ***** ###

import nest_asyncio
nest_asyncio.apply()


# -------------------------------------------------------------------
# Establecer rutas necesarias
# -------------------------------------------------------------------

# Importar y filtrar reglas en español

regla_es = pd.read_excel(load_rules, sheet_name="Español")
regla_es['regla']=regla_es['regla'].str.strip()

# Importar y filtrar reglas en ingles

regla_en = pd.read_excel(load_rules, sheet_name="Ingles")
regla_en['regla']=regla_en['regla'].str.strip()

#regla_en_prueba = regla_en.drop(regla_en.index[6:])
#regla_es_prueba = regla_es.drop(regla_es.index[6:])

# -------------------------------------------------------------------
# Establecer rutas donde se almacenan los datos descargados de Tweet
# -------------------------------------------------------------------

path_es = download_spanish
path_en = download_english

# -------------------------------------------------------------------
#Función principal para descargar Tweets
#segun reglas en formato .CSV
# -------------------------------------------------------------------

def tweets(path, reglas):
    print('Início de Descarga segun regla %s ...' % load_rules )
    # -----------------------------------------------------------------
    #Determinar la fecha desde y hasta cuando se descargaran los tweets
    #(en este caso desde el dia anterios hasta el
    #    momento en que se corre el script)
    # -----------------------------------------------------------------
    d = datetime.date.today()
    dt = d - timedelta(days=1)
    d=str(d)
    dt=str(dt)
    
    # Crear directorios para almacenar descargas 
    path= path+d+'/'
    try:
        os.mkdir(path)
    except OSError:
        print ("Creación de Directorio %s fallo" % path)
    else:
        print ("Creación de directorio %s exitoso " % path)

    #Conteo para realizar la busqueda tres veces, dado que algunas veces devuelve nada.
    count=0
    while count<4:
        #Recorrer reglas
        for index, row in reglas.iterrows():
            #print('>>: ', row)
            #Crear archivo para guardar resultado de regla
            filename = path+str(row["No."])+"-"+d+".csv"
            #Configurar objeto TWINT
            c = twint.Config()
            #Fecha desde cuando se descargaran tweets
            c.Since = dt
            #Asignar regla como argumento para búsqueda en Twitter
            c.Search = row["regla"]
            #Activar descarga en formato CSV segun TWINT
            c.Store_csv = True
            #Contar Cuantos Tweets se descargan por cada Búsqueda
            c.Count = True
            #Si el archivo existe indica que ya se realizo la búsqueda e ira a la suiguiente
            if os.path.exists(filename):
                continue
                #print('Esta busqueda ya fue guardada ' + str(row_es["No."])+": "+row_es["regla"] )
            else:
                #Sino existe el archivo realiza la búsqueda
                #Descarga resultado de la búsqueda en el directorio antes creado
                c.Output = filename
                #Limite maximo de tweets por cada búsqueda
                c.Limit = 10000
                #Ocultar salida
                c.Hide_output = True
                #Correr búsqueda
                with Capturing() as output:
                    twint.run.Search(c)

                if 'Expecting value: line 1 column 1 (char 0) [x] run.Feed' in output:
                    m = random.randrange(240, 300, 17)
                    k = random.randrange(10, 30, 2)
                    f = random.randrange(180, m,k)
                    #print('EN PAUSA: ',f, 'Segundos')
                    time.sleep(f)

                else:
                    continue
                
                
        #Aumentar contador
        count+=1
        if count == 3: print('Fin de Descarga según regla %s ' % load_rules)
    # -----------------------------------------------------------------
    #Leer todos los archivos .csv creados para integrarlos en uno solo
    # -----------------------------------------------------------------
    all_files = glob.glob(path + '/*.csv')
    lista = []
    #cun=0
    #Convertir los CSV a DataFrame y unirlos a una lista
    for filename in all_files:
        df = pd.read_csv(filename, index_col=None, header=0)
        t=df.shape
        #cun+=t[0]
        lista.append(df)
    #Concatenar los Dataframe en uno solo
    frame = pd.concat(lista, axis=0, ignore_index=True)
    #print('Original: ',frame.shape)
    frame = frame.drop_duplicates('tweet')
    #print('luego del drop duplicate: ', frame.shape)
    #Crear un .CSV con DataFrame mayor
    leng = path.split('/')[-3]
    frame.to_csv(path+'All_Data-'+leng+'-'+d+'.csv.gz', index=False, compression='gzip')
    print('Se guardo consolidado .CSV')

tweets(path_en, regla_en)
tweets(path_es, regla_es)
